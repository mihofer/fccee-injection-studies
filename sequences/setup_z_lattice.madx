
!----------------------------------------------------------------------------------------
! Set up Lattice

SET, FORMAT = "19.15f";

CALL, FILE = "../sequences/FCCee_z_216_nosol_1.seq";

!----------------------------------------------------------------------------------------
! Define Beam
! found in fcc_ee_z.madx on https://gitlab.cern.ch/mihofer/fcc-ee-collimation-lattice/-/blob/master/tests/fcc_ee_z.madx

pbeam := 45.6;      !beam 
EXbeam = 0.27e-9;   !horizontal emittance
EYbeam = 1.0e-12;   !vertical emittance
Nbun := 16640;      !number of bunches
NPar := 1.7e11;     !number of particles per bunch

Ebeam := sqrt( pbeam^2 + emass^2 ); !energy of the beam


// Beam defined without radiation as a start - radiation is turned on later depending on the requirements
BEAM, PARTICLE=POSITRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;



!----------------------------------------------------------------------------------------
! Perform initial Twiss and survey - ideal machine with no radiation

SEQEDIT, SEQUENCE = L000015;
         FLATTEN;
         INSTALL, ELEMENT = INJSEPTMARK, CLASS = MARKER, AT = 5922.736613;
         FLATTEN;
ENDEDIT;

USE, SEQUENCE = L000015;

!----------------------------------------------------------------------------------------
! Load Aperture definitions
CALL, FILE="../sequences/aperture/FCCee_aper_definitions.madx";

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 

SHOW, VOLTCA1SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;

SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

// Place where the initial conditions are saved - used for RF matching later if needed
SAVEBETA, LABEL=B.IP, PLACE=#s, SEQUENCE=L000015;


TWISS, FILE = "twiss_FCCee_z_b1_nottapered.tfs", TOLERANCE=1E-12; ! Twiss without radiation and tapering

!---------------------------------------------------------------------------------------
!Turn on RF and Synchrotron Radiation

// RF back on
VOLTCA1 = VOLTCA1SAVE;

// Turn the beam radiation on. N.B. This simple toggle works only if the sequence is not defined in the orginal beam command.
BEAM, RADIATE=TRUE;

// RF matching
LAGCA1 = 0.5;

MATCH, sequence=L000015, BETA0 = B.IP, tapering;
  VARY, NAME=LAGCA1, step=1.0E-7;
  CONSTRAINT, SEQUENCE=L000015, RANGE=#e, PT=0.0;
  JACOBIAN,TOLERANCE=1.0E-14, CALLS=3000;
ENDMATCH;

// Twiss with tapering
USE, SEQUENCE = L000015;

SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

TWISS, TAPERING, file="twiss_FCCee_z_tapered.tfs";

!-------------------------------------------------------------------------------
! Slice the lattice and save a thin sequence
!-------------------------------------------------------------------------------
// Note: if the tapering was enabled in the previous steps, MAKETHIN will inherit the
// tapered magnetic strenghts (KNTAP) calculated by TWISS and the element strengths (KN) accordingly to produce a tapered thin sequence.
// If any of: RF, beam radiation, tapering; were disabled before, the resulting thin sequence is not tapered.

// Slicing with special attention to IR quads and sextupoles     
SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
SELECT, FLAG=makethin, CLASS=rbend, SLICE = 4;
SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 4;
SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 4;
        
SELECT, FLAG=makethin, PATTERN="QC*", SLICE=20;
SELECT, FLAG=makethin, PATTERN="SY*", SLICE=20;
        
MAKETHIN, SEQUENCE=L000015 STYLE=TEAPOT, MAKEDIPEDGE=false;

USE, SEQUENCE = L000015;
       
TWISS, FILE="twiss_FCCee_z_thin_tapered.tfs", TOLERANCE=1E-12;

SELECT, FLAG = TABLE, COLUMN=NUMBER,TURN,X,PX,Y,PY,T,PT,S,E;
